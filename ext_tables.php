<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'Hochschule.HsRoombooking',
            'Hochschulebooking',
            'HochschuleOG'
        );

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('hs_roombooking', 'Configuration/TypoScript', 'HochschuleOG');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_hsroombooking_domain_model_bookingsystem', 'EXT:hs_roombooking/Resources/Private/Language/locallang_csh_tx_hsroombooking_domain_model_bookingsystem.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_hsroombooking_domain_model_bookingsystem');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_hsroombooking_domain_model_rooms', 'EXT:hs_roombooking/Resources/Private/Language/locallang_csh_tx_hsroombooking_domain_model_rooms.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_hsroombooking_domain_model_rooms');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_hsroombooking_domain_model_roomdetails', 'EXT:hs_roombooking/Resources/Private/Language/locallang_csh_tx_hsroombooking_domain_model_roomdetails.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_hsroombooking_domain_model_roomdetails');

    }
);
