
config.tx_extbase {
  persistence {
    classes {

      TYPO3\CMS\Extbase\Domain\Model\FrontendUser {
        subclasses {
          Tx_HsRoombooking_Person = Hochschule\HsRoombooking\Domain\Model\Person
          
        }
      }
      Hochschule\HsRoombooking\Domain\Model\Person {
        mapping {
          tableName = fe_users
          recordType = Tx_HsRoombooking_Person
        }
      }

    }
  }
}
