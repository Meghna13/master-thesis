<?php

namespace Hochschule\HsRoombooking\Domain\Services;

class FunctionFetcherService {

	public function fetchRoomandLesson() {
		$path = 'http://vorlesungsverzeichnisse.verw.hs-offenburg.de/daVinciIS.dll?type=room&content=xml';
		$objectManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Object\\ObjectManager');
		$roomRepository = $objectManager->get("Hochschule\\HsRoombooking\\Domain\\Repository\\RoomsRepository");
		$GLOBALS['TYPO3_DB']->exec_TRUNCATEquery('tx_hsroombooking_domain_model_rooms');
		//$this->roomRepository->removeAll();
		//get data from DaVinci System
		$xml = simplexml_load_file($path);
		$json = json_encode($xml);
		$xmlArray = json_decode($json, true);
		$serialized_array = serialize($xmlArray);
		$unserialized_array = unserialize($serialized_array);
		$rooms = $unserialized_array['Index']['Rooms']['Room'];
		foreach ($rooms as $room) {
			//$ID='9A1C715A-AECA-4AD9-AEF2-4413F4D92259';
			//$Key=$rooms['Key'];
			$ID = $room['ID'];
			/*if($ID=='B9A69AFE-DD21-4151-A4C8-D1F977800B4B'){
					$ID=next($room['ID']);
				}*/
			$key = $room['Key'];

			if (isset($room['Description'])) {
				$Description = $room['Description'];
			} else {
				$room['Description'] = 'Classroom';
				$Description = $room['Description'];

			}
			$lessonPath = 'http://davinci2.rz.hs-offenburg.de/daVinciIS.dll?content=xml&type=room&id=' . $ID;
			$headersCheck = get_headers($lessonPath);
			if ($headersCheck[0] === 'HTTP/1.1 200 OK') {
				//$readXML=file_get_contents($lessonPath);
				/*if(empty($readXML)) {
							//print_r($ID.' is empty');
							continue;
			*/
				//$lessonxml=simplexml_load_string($readXML, "SimpleXMLElement",LIBXML_NOCDATA);
				$lessonxml = simplexml_load_file($lessonPath);
				$jsonLesson = json_encode($lessonxml);
				$lessonArray = json_decode($jsonLesson, true);
				$serialized_array_lesson = serialize($lessonArray);
				$unserialized_array_lesson = unserialize($serialized_array_lesson);
				$Lessons = $unserialized_array_lesson['Lessons']['Lesson'];
				if (is_array($Lessons[0])) {
					foreach ($Lessons as $Lesson) {
						$fetchXML = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('Hochschule\\HsRoombooking\\Domain\\Model\\Rooms');
						$startdate = ($Lesson['Date']);
						$starttime = ($Lesson['Start']);
						$finishtime = ($Lesson['Finish']);
						$startdatetime = ($startdate . $starttime);
						$finishdatetime = ($startdate . $finishtime);

						$fetchXML->setIsBooked(TRUE);
						//print_r($ID.' '.$startdatetime.' '.$finishdatetime);
						$fetchXML->setRoomKey($key);
						$fetchXML->setRoomtype($Description);
						$fetchXML->setEnddatetime(new \DateTime($finishdatetime));
						$fetchXML->setStartdatetime(new \DateTime($startdatetime));
						$roomRepository->add($fetchXML);
						$roomRepository->persistAll();
					}
				}

			} elseif (empty($Lessons)) {
				$fetchXML = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('Hochschule\\HsRoombooking\\Domain\\Model\\Rooms');
				$fetchXML->setIsBooked(TRUE);
				$fetchXML->setRoomKey($key);
				$fetchXML->setRoomtype($Description);
				$roomRepository->add($fetchXML);
				$roomRepository->persistAll();

			} else {
				$lessonPath = next($lessonPath);
			}
		}
	}
}

?>



