<?php

namespace Hochschule\HsRoombooking\Domain\Services;

class ConfroomFetcherService {

	public function getRoomDetails() {
		$path = '/Users/Meghna/Sites/Conference.XML';
		$objectManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Object\\ObjectManager');
		$roomRepository = $objectManager->get("Hochschule\\HsRoombooking\\Domain\\Repository\\RoomsRepository");
		$xml = simplexml_load_file($path);
		$json = json_encode($xml);
		$xmlArray = json_decode($json, true);
		$serializeArray = serialize($xmlArray);
		//var_dump($serializeArray);
		$unserialize_Array = unserialize($serializeArray);
		//var_dump($unserialize_Array);

		$rooms = $unserialize_Array['Rooms']['Room'];
		//$rooms = $unserialize_Array['Room'][0]['@attributes'];
		//$rooms = $unserialize_Array['Room'][0]['Capacity'];
		//$rooms = $unserialize_Array['Room'][0]['Beamer']['@attributes']['IsAvailable'];
		//$rooms = $unserialize_Array['Room'][0]['@attributes']['Key'];
		//Blackboard IsActive

		#True does not work
		//$bb = $unserialize_Array['Room'][0]['Blackboard']['@attributes']['IsActive'];
		//$blackboard = ((boolean) $bb);
		//var_dump(+$bb);
		#False is working fine(conversion to int)
		//$rooms = $unserialize_Array['Room'][0]['Beamer']['@attributes']['IsAvailable'];
		//var_dump(+$rooms);
		#trying with boolean No attribute in XML-This works
		//$rooms = $unserialize_Array['Room'][0]['Beamer'];
		//$beamer = ((boolean) $rooms);
		//var_dump($beamer);
		#True works
		//$bb = $unserialize_Array['Room'][0]['Blackboard'];
		//$blackboard = ((boolean) $bb);
		//var_dump($blackboard);
		#False does not work
		//$rooms = $unserialize_Array['Room'][0]['Beamer']['@attributes']['IsAvailable'];
		//$beamer = ((boolean) $bb);
		//var_dump($beamer);

		//$extracted_room = extract($rooms);
		//var_dump($extracted_room);
		foreach ($rooms as $room) {
			$fetchXML = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('Hochschule\\HsRoombooking\\Domain\\Model\\Rooms');
			//var_dump($room['@attributes']['Key']);
			//var_dump($room['Capacity']);
			$Key = ($room['@attributes']['Key']);
			$Description = 'Conferenceroom';

			$Start = ($room['start']);
			//var_dump($Start);
			$End = ($room['end']);
			//var_dump($End);

			$Date = ($room['Date']);
			//var_dump($Date);

			$startdatetime = ($Date . $Start);
			//var_dump($startdatetime);

			$finishdatetime = ($Date . $End);
			//var_dump($finishdatetime);

			$fetchXML->setIsBooked(TRUE);
			//print_r($ID.' '.$startdatetime.' '.$finishdatetime);
			$fetchXML->setRoomKey($Key);
			$fetchXML->setRoomtype($Description);
			$fetchXML->setEnddatetime(new \DateTime($finishdatetime));
			$fetchXML->setStartdatetime(new \DateTime($startdatetime));
			$roomRepository->add($fetchXML);
			$roomRepository->persistAll();
			//var_dump($room['Beamer']['@attributes']);
			//var_dump($room['Whiteboard']['@attributes']);
			//var_dump($room['Blackboard']['@attributes']);
		}

	}
}
?>
