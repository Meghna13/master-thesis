<?php

namespace Hochschule\HsRoombooking\Domain\Services;

class RoomDetailsFetcherService {

	public function getRoomDetails() {
		$path = '/Users/Meghna/Sites/Room.XML';
		$objectManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Object\\ObjectManager');
		$roomdetailsRepository = $objectManager->get("Hochschule\\HsRoombooking\\Domain\\Repository\\RoomdetailsRepository");
		$xml = simplexml_load_file($path);
		$json = json_encode($xml);
		$xmlArray = json_decode($json, true);
		$serializeArray = serialize($xmlArray);
		$unserialize_Array = unserialize($serializeArray);
		$rooms = $unserialize_Array['Room'];
		foreach ($rooms as $room) {
			$fetchXML = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('Hochschule\\HsRoombooking\\Domain\\Model\\Roomdetails');
			//var_dump($room['@attributes']['Key']);
			//var_dump($room['Capacity']);
			$Key = ($room['@attributes']['Key']);
			$Capacity = ((int) $room['Capacity']);
			$Type = $room['roomtype'];
			$Beamer = ((boolean) $room['Beamer']);
			$Whiteboard = ((boolean) $room['Whiteboard']);
			$Blackboard = ((boolean) $room['Blackboard']);
			$fetchXML->setRoomKey($Key);
			//print_r($ID.' '.$startdatetime.' '.$finishdatetime);
			$fetchXML->setCapacity($Capacity);
			$fetchXML->setWhiteboard($Whiteboard);
			$fetchXML->setBlackboard($Blackboard);
			$fetchXML->setBeamer($Beamer);
			$fetchXML->setRoomtype($Type);
			$roomdetailsRepository->add($fetchXML);
			$roomdetailsRepository->persistAll();

			//var_dump($room['Beamer']['@attributes']);
			//var_dump($room['Whiteboard']['@attributes']);
			//var_dump($room['Blackboard']['@attributes']);
		}

	}
}
?>
