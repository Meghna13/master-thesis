<?php
namespace Hochschule\HsRoombooking\Domain\Services;

class TimerangeService {
	function create_time_range($start, $end, $interval = '30 mins', $format = '24') {
		$startTime = strtotime($start);
		$endTime = strtotime($end);
		$returnTimeFormat = ($format == '12') ? 'h:i' : 'H:i';

		$current = time();
		$addTime = strtotime('+' . $interval, $current);
		$diff = $addTime - $current;

		$times = array();
		while ($startTime < $endTime) {
			$times[] = date($returnTimeFormat, $startTime);
			$startTime += $diff;
		}
		$times[] = date($returnTimeFormat, $startTime);
		return $times;
	}

}

?>
