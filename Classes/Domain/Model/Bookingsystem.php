<?php
namespace Hochschule\HsRoombooking\Domain\Model;

/***
 *
 * This file is part of the "HochschuleOG" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019 Meghna Anand <meghnaa.anand@gmail.com>
 *
 ***/

/**
 * Bookingsystem
 */
class Bookingsystem extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * conferenceroom
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Hochschule\HsRoombooking\Domain\Model\Conferenceroom>
     * @cascade remove
     */
    protected $conferenceroom = null;

    /**
     * rooms
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Hochschule\HsRoombooking\Domain\Model\Rooms>
     * @cascade remove
     */
    protected $rooms = null;

    /**
     * roomDetails
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Hochschule\HsRoombooking\Domain\Model\Roomdetails>
     * @cascade remove
     */
    protected $roomDetails = null;

    /**
     * __construct
     */
    public function __construct()
    {
        //Do not remove the next line: It would break the functionality
        $this->initStorageObjects();
    }

    /**
     * Initializes all ObjectStorage properties
     * Do not modify this method!
     * It will be rewritten on each save in the extension builder
     * You may modify the constructor of this class instead
     *
     * @return void
     */
    protected function initStorageObjects()
    {
        $this->rooms = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $this->roomDetails = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }

    /**
     * Adds a Rooms
     *
     * @param \Hochschule\HsRoombooking\Domain\Model\Rooms $room
     * @return void
     */
    public function addRoom(\Hochschule\HsRoombooking\Domain\Model\Rooms $room)
    {
        $this->rooms->attach($room);
    }

    /**
     * Removes a Rooms
     *
     * @param \Hochschule\HsRoombooking\Domain\Model\Rooms $roomToRemove The Rooms to be removed
     * @return void
     */
    public function removeRoom(\Hochschule\HsRoombooking\Domain\Model\Rooms $roomToRemove)
    {
        $this->rooms->detach($roomToRemove);
    }

    /**
     * Returns the rooms
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Hochschule\HsRoombooking\Domain\Model\Rooms> $rooms
     */
    public function getRooms()
    {
        return $this->rooms;
    }

    /**
     * Sets the rooms
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Hochschule\HsRoombooking\Domain\Model\Rooms> $rooms
     * @return void
     */
    public function setRooms(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $rooms)
    {
        $this->rooms = $rooms;
    }

    /**
     * Adds a Roomdetails
     *
     * @param \Hochschule\HsRoombooking\Domain\Model\Roomdetails $roomDetail
     * @return void
     */
    public function addRoomDetail(\Hochschule\HsRoombooking\Domain\Model\Roomdetails $roomDetail)
    {
        $this->roomDetails->attach($roomDetail);
    }

    /**
     * Removes a Roomdetails
     *
     * @param \Hochschule\HsRoombooking\Domain\Model\Roomdetails $roomDetailToRemove The Roomdetails to be removed
     * @return void
     */
    public function removeRoomDetail(\Hochschule\HsRoombooking\Domain\Model\Roomdetails $roomDetailToRemove)
    {
        $this->roomDetails->detach($roomDetailToRemove);
    }

    /**
     * Returns the roomDetails
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Hochschule\HsRoombooking\Domain\Model\Roomdetails> $roomDetails
     */
    public function getRoomDetails()
    {
        return $this->roomDetails;
    }

    /**
     * Sets the roomDetails
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Hochschule\HsRoombooking\Domain\Model\Roomdetails> $roomDetails
     * @return void
     */
    public function setRoomDetails(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $roomDetails)
    {
        $this->roomDetails = $roomDetails;
    }

    /**
     * Adds a Classroom
     *
     * @param \Hochschule\HsRoombooking\Domain\Model\Conferenceroom $conferenceroom
     * @return void
     */
    public function addConferenceroom(\Hochschule\HsRoombooking\Domain\Model\Conferenceroom $conferenceroom)
    {
        $this->conferenceroom->attach($conferenceroom);
    }

    /**
     * Removes a Classroom
     *
     * @param \Hochschule\HsRoombooking\Domain\Model\Conferenceroom $conferenceroomToRemove The Conferenceroom to be removed
     * @return void
     */
    public function removeConferenceroom(\Hochschule\HsRoombooking\Domain\Model\Conferenceroom $conferenceroomToRemove)
    {
        $this->conferenceroom->detach($conferenceroomToRemove);
    }

    /**
     * Returns the conferenceroom
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Hochschule\HsRoombooking\Domain\Model\Conferenceroom> conferenceroom
     */
    public function getConferenceroom()
    {
        return $this->conferenceroom;
    }

    /**
     * Sets the conferenceroom
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Hochschule\HsRoombooking\Domain\Model\Conferenceroom> $conferenceroom
     * @return void
     */
    public function setConferenceroom(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $conferenceroom)
    {
        $this->conferenceroom = $conferenceroom;
    }
}
