<?php
namespace Hochschule\HsRoombooking\Domain\Model;

/***
 *
 * This file is part of the "HochschuleOG" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019 Meghna Anand <meghnaa.anand@gmail.com>
 *
 ***/

/**
 * Rooms
 */
class Rooms extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * startdatetime
     *
     * @var \DateTime
     */
    protected $startdatetime = null;

    /**
     * enddatetime
     *
     * @var \DateTime
     */
    protected $enddatetime = null;

    /**
     * roomKey
     *
     * @var string
     */
    protected $roomKey = '';

    /**
     * roomtype
     *
     * @var string
     */
    protected $roomtype = '';

    /**
     * isBooked
     *
     * @var bool
     */
    protected $isBooked = '';

    /**
     * roomdetails
     *
     * @var \Hochschule\HsRoombooking\Domain\Model\Roomdetails
     */
    protected $roomdetails = null;

    /**
     * Returns the startdatetime
     *
     * @return \DateTime $startdatetime
     */
    public function getStartdatetime()
    {
        return $this->startdatetime;
    }

    /**
     * Sets the startdatetime
     *
     * @param \DateTime $startdatetime
     * @return void
     */
    public function setStartdatetime(\DateTime $startdatetime)
    {
        $this->startdatetime = $startdatetime;
    }

    /**
     * Returns the enddatetime
     *
     * @return \DateTime $enddatetime
     */
    public function getEnddatetime()
    {
        return $this->enddatetime;
    }

    /**
     * Sets the enddatetime
     *
     * @param \DateTime $enddatetime
     * @return void
     */
    public function setEnddatetime(\DateTime $enddatetime)
    {
        $this->enddatetime = $enddatetime;
    }

    /**
     * Returns the roomKey
     *
     * @return string $roomKey
     */
    public function getRoomKey()
    {
        return $this->roomKey;
    }

    /**
     * Sets the roomKey
     *
     * @param string $roomKey
     * @return void
     */
    public function setRoomKey($roomKey)
    {
        $this->roomKey = $roomKey;
    }

    /**
     * Returns the roomtype
     *
     * @return string $roomtype
     */
    public function getRoomtype()
    {
        return $this->roomtype;
    }

    /**
     * Sets the roomtype
     *
     * @param string $roomtype
     * @return void
     */
    public function setRoomtype($roomtype)
    {
        $this->roomtype = $roomtype;
    }

    /**
     * Returns the roomdetails
     *
     * @return \Hochschule\HsRoombooking\Domain\Model\Roomdetails $roomdetails
     */
    public function getRoomdetails()
    {
        return $this->roomdetails;
    }

    /**
     * Sets the roomdetails
     *
     * @param \Hochschule\HsRoombooking\Domain\Model\Roomdetails $roomdetails
     * @return void
     */
    public function setRoomdetails(\Hochschule\HsRoombooking\Domain\Model\Roomdetails $roomdetails)
    {
        $this->roomdetails = $roomdetails;
    }

    /**
     * Returns the isBooked
     *
     * @return bool isBooked
     */
    public function getIsBooked()
    {
        return $this->isBooked;
    }

    /**
     * Sets the isBooked
     *
     * @param string $isBooked
     * @return void
     */
    public function setIsBooked($isBooked)
    {
        $this->isBooked = $isBooked;
    }

    /**
     * Returns the boolean state of isBooked
     *
     * @return bool
     */
    public function isIsBooked()
    {
        return $this->isBooked;
    }
}
