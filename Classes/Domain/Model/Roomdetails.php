<?php
namespace Hochschule\HsRoombooking\Domain\Model;

/***
 *
 * This file is part of the "HochschuleOG" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019 Meghna Anand <meghnaa.anand@gmail.com>
 *
 ***/

/**
 * Roomdetails
 */
class Roomdetails extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * roomKey
     *
     * @var string
     */
    protected $roomKey = '';

    /**
     * capacity
     *
     * @var int
     */
    protected $capacity = 0;

    /**
     * whiteboard
     *
     * @var bool
     */
    protected $whiteboard = false;

    /**
     * blackboard
     *
     * @var bool
     */
    protected $blackboard = false;

    /**
     * beamer
     *
     * @var bool
     */
    protected $beamer = false;

    /**
     * roomtype
     *
     * @var string
     */
    protected $roomtype = '';

    /**
     * Returns the roomKey
     *
     * @return string $roomKey
     */
    public function getRoomKey()
    {
        return $this->roomKey;
    }

    /**
     * Sets the roomKey
     *
     * @param string $roomKey
     * @return void
     */
    public function setRoomKey($roomKey)
    {
        $this->roomKey = $roomKey;
    }

    /**
     * Returns the capacity
     *
     * @return int $capacity
     */
    public function getCapacity()
    {
        return $this->capacity;
    }

    /**
     * Sets the capacity
     *
     * @param int $capacity
     * @return void
     */
    public function setCapacity($capacity)
    {
        $this->capacity = $capacity;
    }

    /**
     * Returns the whiteboard
     *
     * @return bool $whiteboard
     */
    public function getWhiteboard()
    {
        return $this->whiteboard;
    }

    /**
     * Sets the whiteboard
     *
     * @param bool $whiteboard
     * @return void
     */
    public function setWhiteboard($whiteboard)
    {
        $this->whiteboard = $whiteboard;
    }

    /**
     * Returns the boolean state of whiteboard
     *
     * @return bool
     */
    public function isWhiteboard()
    {
        return $this->whiteboard;
    }

    /**
     * Returns the blackboard
     *
     * @return bool $blackboard
     */
    public function getBlackboard()
    {
        return $this->blackboard;
    }

    /**
     * Sets the blackboard
     *
     * @param bool $blackboard
     * @return void
     */
    public function setBlackboard($blackboard)
    {
        $this->blackboard = $blackboard;
    }

    /**
     * Returns the boolean state of blackboard
     *
     * @return bool
     */
    public function isBlackboard()
    {
        return $this->blackboard;
    }

    /**
     * Returns the beamer
     *
     * @return bool $beamer
     */
    public function getBeamer()
    {
        return $this->beamer;
    }

    /**
     * Sets the beamer
     *
     * @param bool $beamer
     * @return void
     */
    public function setBeamer($beamer)
    {
        $this->beamer = $beamer;
    }

    /**
     * Returns the boolean state of beamer
     *
     * @return bool
     */
    public function isBeamer()
    {
        return $this->beamer;
    }

    /**
     * Returns the roomtype
     *
     * @return string $roomtype
     */
    public function getRoomtype()
    {
        return $this->roomtype;
    }

    /**
     * Sets the roomtype
     *
     * @param string $roomtype
     * @return void
     */
    public function setRoomtype($roomtype)
    {
        $this->roomtype = $roomtype;
    }
}
