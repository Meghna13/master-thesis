<?php
namespace Hochschule\HsRoombooking\Domain\Repository;

/***
 *
 * This file is part of the "HochschuleOG" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019 Meghna Anand <meghnaa.anand@gmail.com>
 *
 ***/

/**
 * The repository for Rooms
 */
class RoomsRepository extends \TYPO3\CMS\Extbase\Persistence\Repository {
	public function persistAll() {
		$this->persistenceManager->persistAll();
	}

	/**
	 * @param $startdatetime
	 * @param $enddatetime
	 * @param $Key
	 */
	public function getAvailableRoomKeys($startdatetime, $enddatetime, $Key) {
		$a_rooms = [];
		$query = $this->createQuery();
		$query->matching($query->logicalAnd($query->equals('roomKey', $Key), $query->logicalOR($query->logicalAnd($query->greaterThan('startdatetime', $startdatetime), $query->lessThan('startdatetime', $enddatetime)), $query->logicalAnd($query->greaterThan('enddatetime', $startdatetime), $query->lessThan('enddatetime', $enddatetime)), $query->logicalAnd($query->lessThanOrEqual('startdatetime', $startdatetime), $query->greaterThanOrEqual('enddatetime', $enddatetime)))));
		/*$query->statement('SELECT * from tx_hsroombooking_domain_model_confroom WHERE
                                                                                                                                                						(startdatetime >=? AND startdatetime<=?) OR (enddatetime>=? AND enddatetime<=?)OR(startdatetime<=? AND enddatetime>=?) AND ', ['$startdatetime', '$startdatetime'], ['$enddatetime', '$enddatetime'], ['$startdatetime', '$enddatetime']);*/

		//$result = $query->execute();
		$result = $query->execute()->count();
		return $result;
	}

	/**
	 * @param $bookedValue
	 */
	public function getRequestedRooms($bookedValue) {
		$query = $this->createQuery();
		$query->matching($query->equals('is_booked', $bookedValue));
		$result = $query->execute();
		return $result;
	}

	/**
	 * @param $uid
	 */
	public function findByIdAccepted($uid) {
		$query = $this->createQuery();
		$query->matching($query->equals('uid', $uid));
		$result = $query->execute();
		return $result;
	}
}
