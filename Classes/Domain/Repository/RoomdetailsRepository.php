<?php
namespace Hochschule\HsRoombooking\Domain\Repository;

/***
 *
 * This file is part of the "HochschuleOG" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019 Meghna Anand <meghnaa.anand@gmail.com>
 *
 ***/

/**
 * The repository for Roomdetails
 */
class RoomdetailsRepository extends \TYPO3\CMS\Extbase\Persistence\Repository {
	public function persistAll() {
		$this->persistenceManager->persistAll();
	}

	/**
	 * @param $roomtype
	 */
	public function fetchRoomkey($roomtype) {
		$query = $this->createQuery();
		$query->matching($query->equals('roomtype', $roomtype));
		$result = $query->execute();
		foreach ($result as $rooms) {
			$unique_rooms[] = $rooms->getRoomKey();
		}
		return $unique_rooms;
	}

	/**
	 * @param $key
	 */
	public function getRoomDetails($key) {
		$query = $this->createQuery();
		$query->matching($query->in('roomKey', $key));
		$result = $query->execute();
		return $result;
	}
}
