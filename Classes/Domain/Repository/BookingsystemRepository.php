<?php
namespace Hochschule\HsRoombooking\Domain\Repository;

/***
 *
 * This file is part of the "HochschuleOG" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019 Meghna Anand <meghnaa.anand@gmail.com>
 *
 ***/

/**
 * The repository for Bookingsystems
 */
class BookingsystemRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{
    }
