<?php
namespace Hochschule\HsRoombooking\Domain\Session;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2019 Meghna Anand <manand@stud.hs-offenburg.de>, Hochschule Offenburg
 *
 *  All rights reserved
 *
 *  Based on Alexander Dick's Backend session handler mit extbase
 *  http://www.adick.at/2012/02/18/backend-session-handler-mit-extbase/
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * FrontendSessionHandler
 */
class FrontendSessionHandler extends \TYPO3\CMS\Extbase\Persistence\Repository {

	protected $storageKey = "tx_hochschule";

	/**
	 * @param string $storageKey
	 */

	public function setStorageKey($storageKey) {
		$this->storageKey = $storageKey;
	}

	/**
	 * Store the variable in the session
	 * @param string $key
	 * @param mixed $value
	 * @return boolean
	 */
	public function store($key, $value) {
		$data = $GLOBALS['TSFE']->fe_user->getKey('ses', $this->storageKey);
		$data[$key] = serialize($value);
		$ret = $GLOBALS['TSFE']->fe_user->setKey('ses', $this->storageKey, $data);
		$GLOBALS["TSFE"]->fe_user->sesData_change = true;
		$GLOBALS['TSFE']->fe_user->storeSessionData();
		return $ret;

	}

	/**
	 * Delete the session variable
	 * @param string $key
	 * @return boolean
	 */
	public function delete($key) {
		$data = $GLOBALS['TSFE']->fe_user->getKey('ses', $this->storageKey);
		unset($data[$key]);
		$ret = $GLOBALS['TSFE']->fe_user->setKey('ses', $this->storageKey, $data);
		$GLOBALS["TSFE"]->fe_user->sesData_change = true;
		$GLOBALS['TSFE']->fe_user->storeSessionData();
		return $ret;
	}

	/**
	 * Wert auslesen
	 * @param string $key
	 * @return mixed
	 */
	public function get($key) {
		$data = $GLOBALS['TSFE']->fe_user->getKey('ses', $this->storageKey);
		return isset($data[$key]) ? unserialize($data[$key]) : NULL;
	}
}
?>