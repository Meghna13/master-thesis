<?php
namespace Hochschule\HsRoombooking\Controller;

/***
 *
 * This file is part of the "HochschuleOG" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019 Meghna Anand <meghnaa.anand@gmail.com>
 *
 ***/

/**
 * RoomdetailsController
 */
class RoomdetailsController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * roomdetailsRepository
     *
     * @var \Hochschule\HsRoombooking\Domain\Repository\RoomdetailsRepository
     * @inject
     */
    protected $roomdetailsRepository = null;

    /**
     * action list
     *
     * @return void
     */
    public function listAction()
    {
        $roomdetails = $this->roomdetailsRepository->findAll();
        $this->view->assign('roomdetails', $roomdetails);
    }

    /**
     * action show
     *
     * @param \Hochschule\HsRoombooking\Domain\Model\Roomdetails $roomdetails
     * @return void
     */
    public function showAction(\Hochschule\HsRoombooking\Domain\Model\Roomdetails $roomdetails)
    {
        $this->view->assign('roomdetails', $roomdetails);
    }

    /**
     * action new
     *
     * @return void
     */
    public function newAction()
    {

    }

    /**
     * action create
     *
     * @param \Hochschule\HsRoombooking\Domain\Model\Roomdetails $newRoomdetails
     * @return void
     */
    public function createAction(\Hochschule\HsRoombooking\Domain\Model\Roomdetails $newRoomdetails)
    {
        $this->addFlashMessage('The object was created. Please be aware that this action is publicly accessible unless you implement an access check. See https://docs.typo3.org/typo3cms/extensions/extension_builder/User/Index.html', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::WARNING);
        $this->roomdetailsRepository->add($newRoomdetails);
        $this->redirect('list');
    }

    /**
     * action edit
     *
     * @param \Hochschule\HsRoombooking\Domain\Model\Roomdetails $roomdetails
     * @ignorevalidation $roomdetails
     * @return void
     */
    public function editAction(\Hochschule\HsRoombooking\Domain\Model\Roomdetails $roomdetails)
    {
        $this->view->assign('roomdetails', $roomdetails);
    }

    /**
     * action update
     *
     * @param \Hochschule\HsRoombooking\Domain\Model\Roomdetails $roomdetails
     * @return void
     */
    public function updateAction(\Hochschule\HsRoombooking\Domain\Model\Roomdetails $roomdetails)
    {
        $this->addFlashMessage('The object was updated. Please be aware that this action is publicly accessible unless you implement an access check. See https://docs.typo3.org/typo3cms/extensions/extension_builder/User/Index.html', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::WARNING);
        $this->roomdetailsRepository->update($roomdetails);
        $this->redirect('list');
    }
}
