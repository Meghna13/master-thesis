<?php
namespace Hochschule\HsRoombooking\Controller;

/***
 *
 * This file is part of the "HochschuleOG" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019 Meghna Anand <meghnaa.anand@gmail.com>
 *
 ***/

/**
 * RoomsController
 */
class RoomsController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {
	/**
	 * roomsRepository
	 *
	 * @var \Hochschule\HsRoombooking\Domain\Repository\RoomsRepository
	 * @inject
	 */
	protected $roomsRepository = null;

	/**
	 * action list
	 *
	 * @return void
	 */
	public function listAction() {
		$rooms = $this->roomsRepository->findAll();
		$this->view->assign('rooms', $rooms);
	}

	/**
	 * action show
	 *
	 * @param \Hochschule\HsRoombooking\Domain\Model\Rooms $rooms
	 * @return void
	 */
	public function showAction(\Hochschule\HsRoombooking\Domain\Model\Rooms $rooms) {
		$this->view->assign('rooms', $rooms);
	}

	/**
	 * action new
	 *
	 * @return void
	 */
	public function newAction() {

	}

	/**
	 * action create
	 *
	 * @param \Hochschule\HsRoombooking\Domain\Model\Rooms $newRooms
	 * @return void
	 */
	public function createAction(\Hochschule\HsRoombooking\Domain\Model\Rooms $newRooms) {
		$this->addFlashMessage('The object was created. Please be aware that this action is publicly accessible unless you implement an access check. See https://docs.typo3.org/typo3cms/extensions/extension_builder/User/Index.html', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::WARNING);
		$this->roomsRepository->add($newRooms);
		$this->redirect('list');
	}

	/**
	 * action edit
	 *
	 * @param \Hochschule\HsRoombooking\Domain\Model\Rooms $rooms
	 * @ignorevalidation $rooms
	 * @return void
	 */
	public function editAction(\Hochschule\HsRoombooking\Domain\Model\Rooms $rooms) {
		$this->view->assign('rooms', $rooms);
	}

	/**
	 * action update
	 *
	 * @param \Hochschule\HsRoombooking\Domain\Model\Rooms $rooms
	 * @return void
	 */
	public function updateAction(\Hochschule\HsRoombooking\Domain\Model\Rooms $rooms) {
		$this->addFlashMessage('The object was updated. Please be aware that this action is publicly accessible unless you implement an access check. See https://docs.typo3.org/typo3cms/extensions/extension_builder/User/Index.html', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::WARNING);
		$this->roomsRepository->update($rooms);
		$this->redirect('list');
	}

	/**
	 * action list
	 *
	 * @return void
	 */
	public function rejectedAction() {

	}

}
