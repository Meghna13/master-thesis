<?php
namespace Hochschule\HsRoombooking\Controller;

/***
 *
 * This file is part of the "HochschuleOG" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019 Meghna Anand <meghnaa.anand@gmail.com>
 *
 ***/

/**
 * BookingsystemController
 */
class BookingsystemController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {
	/**
	 * @var \TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager
	 * @inject
	 */
	protected $persistenceManager = null;

	/**
	 * @var \Hochschule\HsRoombooking\Domain\Session\FrontendSessionHandler
	 * @inject
	 */
	protected $frontendSession = NULL;

	/**
	 * bookingsystemRepository
	 *
	 * @var \Hochschule\HsRoombooking\Domain\Repository\BookingsystemRepository
	 * @inject
	 */
	protected $bookingsystemRepository = null;

	/**
	 * roomsRepository
	 *
	 * @var \Hochschule\HsRoombooking\Domain\Repository\RoomsRepository
	 * @inject
	 */
	protected $roomsRepository = null;
	/**
	 * roomdetailsRepository
	 *
	 * @var \Hochschule\HsRoombooking\Domain\Repository\RoomdetailsRepository
	 * @inject
	 */
	protected $roomdetailsRepository = null;
	/**
	 * frontendUserRepository
	 *
	 * @var \TYPO3\CMS\Extbase\Domain\Repository\FrontendUserRepository
	 * @inject
	 */
	protected $frontendUserRepository = null;

	/**
	 * action list
	 *
	 * @return void
	 */
	public function listAction() {
		//check user group on Login, 2 is for authorized user, 1 is for requestor
		if ($GLOBALS['TSFE']->fe_user->user['usergroup'] == 2) {
			$this->forward('authorized');
		} else {
			//for dropdown for time
			$timerange = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('Hochschule\\HsRoombooking\\Domain\\Services\\TimerangeService');
			$timerangedatas = $timerange->create_time_range('08:00', '19:00', '30 min');
			//room types
			$roomtypes_array = ['Classroom', 'Seminarraum', 'Grosser Hörsaal', 'Conferenceroom'];
			if ($this->request->hasArgument('From') && $this->request->hasArgument('To') && $this->request->hasArgument('Date') && $this->request->hasArgument('roomtype')) {
				$date = $this->request->getArgument('Date');
				$from = $this->request->getArgument('From');
				$to = $this->request->getArgument('To');
				if ($from > $to) {
					$this->redirect('list');
					var_dump('Please give value of From less than To');
				}
				$type = $this->request->getArgument('roomtype');
				if ($type == 'Classroom' || $type == 'Seminarraum' || $type == 'Grosser Hörsaal' || ($type = 'Conferenceroom')) {
					$startdatetime = $date . ' ' . $from;
					$enddatetime = $date . ' ' . $to;
					$availableRoomKey = [];
					//get all unique room key of the roomytype
					$roomKeys = $this->roomdetailsRepository->fetchRoomkey($type);
					foreach ($roomKeys as $roomKey) {
						$count = $this->roomsRepository->getAvailableRoomKeys($startdatetime, $enddatetime, $roomKey);
						//all room with count=0 are available rooms
						if ($count == 0) {
							array_push($availableRoomKey, $roomKey);
						}
					}
					$roomDetails = $this->roomdetailsRepository->getRoomDetails($availableRoomKey);
					$this->view->assign('roomDetails', $roomDetails);
					//store filters in session
					$To = $this->request->getArgument('To');
					$this->frontendSession->store('To', $To);
					$From = $this->request->getArgument('From');
					$this->frontendSession->store('From', $From);
					$Date = $this->request->getArgument('Date');
					$this->frontendSession->store('Date', $Date);
					$RoomType = $this->request->getArgument('roomtype');
					$this->frontendSession->store('RoomType', $RoomType);
				}
			}
			$this->view->assign('timerangedatas', $timerangedatas);
			$this->view->assign('roomtypes', $roomtypes_array);
			$this->view->assign('bookingsystems', $bookingsystems);
			$this->view->assign('getPersonUid', $getPersonUid);
		}
	}

	/**
	 * action show
	 *
	 * @param \Hochschule\HsRoombooking\Domain\Model\Bookingsystem $bookingsystem
	 * @return void
	 */
	public function showAction(\Hochschule\HsRoombooking\Domain\Model\Bookingsystem $bookingsystem) {
		$this->view->assign('bookingsystem', $bookingsystem);
	}

	/**
	 * action new
	 *
	 * @return void
	 */
	public function newAction() {

	}

	/**
	 * action create
	 *
	 * @param \Hochschule\HsRoombooking\Domain\Model\Bookingsystem $newBookingsystem
	 * @return void
	 */
	public function createAction(\Hochschule\HsRoombooking\Domain\Model\Bookingsystem $newBookingsystem) {
		$this->addFlashMessage('The object was created. Please be aware that this action is publicly accessible unless you implement an access check. See https://docs.typo3.org/typo3cms/extensions/extension_builder/User/Index.html', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::WARNING);
		$this->bookingsystemRepository->add($newBookingsystem);
		$this->redirect('list');
	}

	/**
	 * action edit
	 *
	 * @param \Hochschule\HsRoombooking\Domain\Model\Bookingsystem $bookingsystem
	 * @ignorevalidation $bookingsystem
	 * @return void
	 */
	public function editAction(\Hochschule\HsRoombooking\Domain\Model\Bookingsystem $bookingsystem) {
		$this->view->assign('bookingsystem', $bookingsystem);
	}

	/**
	 * action update
	 *
	 * @param \Hochschule\HsRoombooking\Domain\Model\Bookingsystem $bookingsystem
	 * @return void
	 */
	public function updateAction(\Hochschule\HsRoombooking\Domain\Model\Bookingsystem $bookingsystem) {
		$this->addFlashMessage('The object was updated. Please be aware that this action is publicly accessible unless you implement an access check. See https://docs.typo3.org/typo3cms/extensions/extension_builder/User/Index.html', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::WARNING);
		$this->bookingsystemRepository->update($bookingsystem);
		$this->redirect('list');
	}

	/**
	 * action delete
	 *
	 * @param \Hochschule\HsRoombooking\Domain\Model\Bookingsystem $bookingsystem
	 * @return void
	 */
	public function deleteAction(\Hochschule\HsRoombooking\Domain\Model\Bookingsystem $bookingsystem) {
		$this->addFlashMessage('The object was deleted. Please be aware that this action is publicly accessible unless you implement an access check. See https://docs.typo3.org/typo3cms/extensions/extension_builder/User/Index.html', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::WARNING);
		$this->bookingsystemRepository->remove($bookingsystem);
		$this->redirect('list');
	}

	/**
	 * action book
	 *
	 * @return void
	 */
	public function bookAction() {
		$roomKey = $this->request->getArgument('selectedKey');
		$this->frontendSession->store('selectedKey', $roomKey);
		$this->redirect('confirmation');
	}

	/**
	 * action confirmation
	 *
	 * @return void
	 */
	public function confirmationAction() {
		$roomKey_selected = $this->frontendSession->get('selectedKey');
		$requestedFnameValue = $GLOBALS['TSFE']->fe_user->user['first_name'];
		$requestedLnameValue = $GLOBALS['TSFE']->fe_user->user['last_name'];
		$requestedEmailValue = $GLOBALS['TSFE']->fe_user->user['email'];
		$this->view->assign('requestedFnameValue', $requestedFnameValue);
		$this->view->assign('requestedLnameValue', $requestedLnameValue);
		$this->view->assign('requestedEmailValue', $requestedEmailValue);
		$this->view->assign('roomKey_selected', $roomKey_selected);
	}

	/**
	 * action user
	 *
	 * @return void
	 */
	public function userAction() {
		$message = $this->request->getArgument('comments');
		var_dump($message);
		$this->frontendSession->store('message', $message);
		$this->redirect('completed');
	}

	/**
	 * action completed
	 *
	 * @return void
	 */
	public function completedAction() {
		$message = $this->frontendSession->get('message');
		$roomKey = $this->frontendSession->get('selectedKey');
		$Firstname = $GLOBALS['TSFE']->fe_user->user['first_name'];
		$Lastname = $GLOBALS['TSFE']->fe_user->user['last_name'];
		$Email = $GLOBALS['TSFE']->fe_user->user['email'];
		$To = $this->frontendSession->get('To');
		$From = $this->frontendSession->get('From');
		$Date = $this->frontendSession->get('Date');
		$roomtype = $this->frontendSession->get('RoomType');
		$start = $Date . ' ' . $From;
		$End = $Date . ' ' . $To;
		$mail = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Mail\MailMessage::class);
		$mail->setSubject('Roombooking Request')->setFrom(['manik.mayur@hs-offenburg.de' => 'Meghna Anand'])->setTo(['manik.mayur@hs-offenburg.de', 'meghnaa.anand@gmail.com' => 'A name'])->setBody('Request Received')->addPart('<q>Roombooking Request has been made</q>', 'text/html')->send();
		$Rooms = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('Hochschule\\HsRoombooking\\Domain\\Model\\Rooms');
		$Rooms->setStartdatetime(new \DateTime($start));
		$Rooms->setEnddatetime(new \DateTime($End));
		$Rooms->setRoomKey($roomKey);
		$Rooms->setRoomtype($roomtype);
		$Rooms->setIsBooked(FALSE);
		$this->roomsRepository->add($Rooms);
		$this->view->assign('roomKey', $roomKey);
		$this->view->assign('Firstname', $Firstname);
		$this->view->assign('Lastname', $Lastname);
		$this->view->assign('Email', $Email);
		$this->view->assign('start', $start);
		$this->view->assign('end', $End);
	}

	/**
	 * action authorized
	 *
	 * @return void
	 */
	public function authorizedAction() {
		$rooms = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('Hochschule\\HsRoombooking\\Domain\\Model\\Rooms');
		$nameAuthorizedUser = $this->frontendSession->get('Name');
		$emailOfRequestor = $this->frontendSession->get('Email');
		$bookedValue = 0;
		$start = [];
		$End = [];
		//get all rooms with is_booked=0, requested status
		$requestedRooms = $this->roomsRepository->getRequestedRooms($bookedValue);
		foreach ($requestedRooms as $rooms) {
			$start = $rooms->getStartdatetime()->format('Y-m-d H:i:s');
			$End = $rooms->getEnddatetime()->format('Y-m-d H:i:s');
		}
		$this->view->assign('end', $End);
		$this->view->assign('start', $start);
		$this->view->assign('name', $nameAuthorizedUser);
		$this->view->assign('requestedRooms', $requestedRooms);
	}

	/**
	 * action accepted
	 *
	 * @return void
	 */
	public function acceptedAction() {
		$roomKeyAccepted = $this->frontendSession->get('roomKeyAccepted');
		$startdatetimeAccepted = $this->frontendSession->get('startdatetimeAccepted');
		$enddatetimeAccepted = $this->frontendSession->get('enddatetimeAccepted');
		$roomtypeAccepted = $this->frontendSession->get('roomtypeAccepted');
		$roomIDAccepted = $this->frontendSession->get('roomIDAccepted');
		$roomsAccepted = $this->roomsRepository->findByIdAccepted($roomIDAccepted);
		foreach ($roomsAccepted as $rooms) {
			$rooms->setIsBooked(TRUE);
			$this->roomsRepository->update($rooms);
		}
		$mail = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Mail\MailMessage::class);
		$mail->setSubject('Roombooking Request')->setFrom(['manik.mayur@hs-offenburg.de' => 'Meghna Anand'])->setTo(['manik.mayur@hs-offenburg.de', 'meghnaa.anand@gmail.com' => 'A name'])->setBody('Request Received')->addPart('<q>Your Request has been Accepted</q>', 'text/html')->send();
		$this->view->assign('roomKeyAccepted', $roomKeyAccepted);
		$this->view->assign('startdatetimeAccepted', $startdatetimeAccepted);
		$this->view->assign('enddatetimeAccepted', $enddatetimeAccepted);
		$this->view->assign('roomtypeAccepted', $roomtypeAccepted);
	}

	/**
	 * action process
	 *
	 * @return void
	 */
	public function processAction() {
		//processAction helps in storage of values in session variable
		if ($this->request->hasArgument('acceptedValue')) {
			$roomKeyAuthSelected = $this->request->getArgument('acceptedValue');
			$thePostIdArray = explode(',', $roomKeyAuthSelected);
			$roomtypeAccepted = $thePostIdArray[3];
			$roomIDAccepted = $thePostIdArray[4];
			$roomKeyAccepted = $thePostIdArray[0];
			$startdatetimeAccepted = $thePostIdArray[1];
			$enddatetimeAccepted = $thePostIdArray[2];
			$this->frontendSession->store('roomKeyAccepted', $roomKeyAccepted);
			$this->frontendSession->store('startdatetimeAccepted', $startdatetimeAccepted);
			$this->frontendSession->store('enddatetimeAccepted', $enddatetimeAccepted);
			$this->frontendSession->store('roomtypeAccepted', $roomtypeAccepted);
			$this->frontendSession->store('roomIDAccepted', $roomIDAccepted);
			$this->redirect('accepted');
		} else {
			if ($this->request->hasArgument('rejectedValue')) {
				$roomKeyAuthSelected = $this->request->getArgument('rejectedValue');
				$thePostIdArray = explode(',', $roomKeyAuthSelected);
				$roomtypeRejected = $thePostIdArray[3];
				$roomIDRejected = $thePostIdArray[4];
				$roomKeyRejected = $thePostIdArray[0];
				$startdatetimeRejected = $thePostIdArray[1];
				$enddatetimeRejected = $thePostIdArray[2];
				$this->frontendSession->store('roomKeyRejected', $roomKeyRejected);
				$this->frontendSession->store('startdatetimeRejected', $startdatetimeRejected);
				$this->frontendSession->store('enddatetimeRejected', $enddatetimeRejected);
				$this->frontendSession->store('roomtypeRejected', $roomtypeRejected);
				$this->frontendSession->store('roomIDRejected', $roomIDRejected);
				$this->redirect('rejected');
			}
		}
	}

	/**
	 * action rejected
	 *
	 * @return void
	 */
	public function rejectedAction() {
		$roomKeyRejected = $this->frontendSession->get('roomKeyRejected');
		$startdatetimeRejected = $this->frontendSession->get('startdatetimeRejected');
		$enddatetimeRejected = $this->frontendSession->get('enddatetimeRejected');
		$roomtypeRejected = $this->frontendSession->get('roomtypeRejected');
		$roomIDRejected = $this->frontendSession->get('roomIDRejected');
		$roomsRejected = $this->roomsRepository->findByIdAccepted($roomIDRejected);
		foreach ($roomsRejected as $rooms) {
			$this->roomsRepository->remove($rooms);
			$this->roomsRepository->persistAll();
		}
		$mail = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Mail\MailMessage::class);
		$mail->setSubject('Roombooking Request')->setFrom(['manik.mayur@hs-offenburg.de' => 'Meghna Anand'])->setTo(['manik.mayur@hs-offenburg.de', 'meghnaa.anand@gmail.com' => 'A name'])->setBody('Request Received')->addPart('<q>Your Request has been rejected</q>', 'text/html')->send();
		$this->view->assign('roomKeyRejected', $roomKeyRejected);
		$this->view->assign('startdatetimeRejected', $startdatetimeRejected);
		$this->view->assign('enddatetimeRejected', $enddatetimeRejected);
		$this->view->assign('roomtypeRejected', $roomtypeRejected);
	}
}
