<?php
namespace Hochschule\HsRoombooking\Command;

//use \TYPO3\CMS\Extbase\Mvc\Controller\CommandController;

class XMLRoomdetailsFetcherCommandController extends \TYPO3\CMS\Extbase\Mvc\Controller\CommandController {
	/**
	 * LoadDetailXMLData
	 *
	 *
	 */
	public function findRoomdetailCommand() {
		$extensionPath = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($this->extensionName);
		$xmlpath = 'Resources/Public/XML/Conference.xml';
		$path = $extensionPath . $xmlpath;
		$objectManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Object\\ObjectManager');
		$roomdetailsRepository = $objectManager->get("Hochschule\\HsRoombooking\\Domain\\Repository\\RoomdetailsRepository");
		$xml = simplexml_load_file($path);
		$json = json_encode($xml);
		$xmlArray = json_decode($json, true);
		$serializeArray = serialize($xmlArray);
		$unserialize_Array = unserialize($serializeArray);
		$rooms = $unserialize_Array['Room'];
		foreach ($rooms as $room) {
			$fetchXML = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('Hochschule\\HsRoombooking\\Domain\\Model\\Roomdetails');
			$Key = ($room['@attributes']['Key']);
			$Capacity = ((int) $room['Capacity']);
			$Type = $room['roomtype'];
			$Beamer = ((boolean) $room['Beamer']);
			$Whiteboard = ((boolean) $room['Whiteboard']);
			$Blackboard = ((boolean) $room['Blackboard']);
			$fetchXML->setRoomKey($Key);
			$fetchXML->setCapacity($Capacity);
			$fetchXML->setWhiteboard($Whiteboard);
			$fetchXML->setBlackboard($Blackboard);
			$fetchXML->setBeamer($Beamer);
			$fetchXML->setRoomtype($Type);
			$roomdetailsRepository->add($fetchXML);
			$roomdetailsRepository->persistAll();
		}

	}
}
?>
