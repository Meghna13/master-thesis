<?php
namespace Hochschule\HsRoombooking\Command;

//use \TYPO3\CMS\Extbase\Mvc\Controller\CommandController;

class XMLConfroomFetcherCommandController extends \TYPO3\CMS\Extbase\Mvc\Controller\CommandController {
	/**
	 * LoadConfXMLData
	 *
	 *
	 */
	public function findBookedConfRoomsCommand() {
		$extensionPath = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($this->extensionName);
		$xmlpath = 'Resources/Public/XML/Conference.xml';
		$path = $extensionPath . $xmlpath;
		$objectManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Object\\ObjectManager');
		$roomRepository = $objectManager->get("Hochschule\\HsRoombooking\\Domain\\Repository\\RoomsRepository");
		$xml = simplexml_load_file($path);
		$json = json_encode($xml);
		$xmlArray = json_decode($json, true);
		$serializeArray = serialize($xmlArray);
		$unserialize_Array = unserialize($serializeArray);
		$rooms = $unserialize_Array['Rooms']['Room'];
		foreach ($rooms as $room) {
			$fetchXML = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('Hochschule\\HsRoombooking\\Domain\\Model\\Rooms');
			$Key = ($room['@attributes']['Key']);
			$Description = 'Conferenceroom';
			$Start = ($room['start']);
			$End = ($room['end']);
			$Date = ($room['Date']);
			$startdatetime = ($Date . $Start);
			$finishdatetime = ($Date . $End);
			$fetchXML->setIsBooked(TRUE);
			$fetchXML->setRoomKey($Key);
			$fetchXML->setRoomtype($Description);
			$fetchXML->setEnddatetime(new \DateTime($finishdatetime));
			$fetchXML->setStartdatetime(new \DateTime($startdatetime));
			$roomRepository->add($fetchXML);
			$roomRepository->persistAll();

		}

	}
}
?>
