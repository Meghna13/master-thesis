<?php
namespace Hochschule\HsRoombooking\Command;

//use \TYPO3\CMS\Extbase\Mvc\Controller\CommandController;

class XMLFetcherCommandController extends \TYPO3\CMS\Extbase\Mvc\Controller\CommandController {
	/**
	 * LoadXMLData
	 *
	 *
	 */
	public function findBookedRoomsCommand() {
		$path = 'http://vorlesungsverzeichnisse.verw.hs-offenburg.de/daVinciIS.dll?type=room&content=xml';
		$objectManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Object\\ObjectManager');
		$roomRepository = $objectManager->get("Hochschule\\HsRoombooking\\Domain\\Repository\\RoomsRepository");
		$GLOBALS['TYPO3_DB']->exec_TRUNCATEquery('tx_hsroombooking_domain_model_rooms');
		$xml = simplexml_load_file($path);
		$json = json_encode($xml);
		$xmlArray = json_decode($json, true);
		$serialized_array = serialize($xmlArraay);
		$unserialized_array = unserialize($serialized_array);
		$rooms = $unserialized_array['Index']['Rooms']['Room'];
		foreach ($rooms as $room) {
			$ID = $room['ID'];
			$key = $room['Key'];
			$lessonPath = 'http://davinci2.rz.hs-offenburg.de/daVinciIS.dll?content=xml&type=room&id=' . $ID;
			$lessonxml = simplexml_load_file($lessonPath);
			$jsonLesson = json_encode($lessonxml);
			$lessonArray = json_decode($jsonLesson, true);
			$serialized_array_lesson = serialize($lessonArray);
			$unserialized_array_lesson = unserialize($serialized_array_lesson);
			$Lessons = $unserialized_array_lesson['Lessons']['Lesson'];
			foreach ($Lessons as $Lesson) {
				$fetchXML = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('Example\\Example\\Domain\\Model\\Classes');
				$startdate = ($Lesson['Date']);
				$starttime = ($Lesson['Start']);
				$finishtime = ($Lesson['Finish']);
				$startdatetime = ($startdate . $starttime);
				$finishdatetime = ($startdate . $finishtime);
				$fetchXML->setRoomID($ID);
				$fetchXML->setRoomKey($key);
				$fetchXML->setEnddatetime(new \DateTime($finishdatetime));
				$fetchXML->setStartdatetime(new \DateTime($startdatetime));
				$classesRepository->add($fetchXML);
				$classesRepository->persistAll();

			}
		}
	}
}

?>