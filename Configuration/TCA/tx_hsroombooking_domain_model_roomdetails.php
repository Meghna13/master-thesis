<?php
return [
    'ctrl' => [
        'title' => 'LLL:EXT:hs_roombooking/Resources/Private/Language/locallang_db.xlf:tx_hsroombooking_domain_model_roomdetails',
        'label' => 'room_key',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'versioningWS' => true,
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ],
        'searchFields' => 'room_key,capacity,whiteboard,blackboard,beamer,roomtype',
        'iconfile' => 'EXT:hs_roombooking/Resources/Public/Icons/tx_hsroombooking_domain_model_roomdetails.gif'
    ],
    'interface' => [
        'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, room_key, capacity, whiteboard, blackboard, beamer, roomtype',
    ],
    'types' => [
        '1' => ['showitem' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, room_key, capacity, whiteboard, blackboard, beamer, roomtype, --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.access, starttime, endtime'],
    ],
    'columns' => [
        'sys_language_uid' => [
            'exclude' => true,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'special' => 'languages',
                'items' => [
                    [
                        'LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages',
                        -1,
                        'flags-multiple'
                    ]
                ],
                'default' => 0,
            ],
        ],
        'l10n_parent' => [
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'exclude' => true,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'default' => 0,
                'items' => [
                    ['', 0],
                ],
                'foreign_table' => 'tx_hsroombooking_domain_model_roomdetails',
                'foreign_table_where' => 'AND tx_hsroombooking_domain_model_roomdetails.pid=###CURRENT_PID### AND tx_hsroombooking_domain_model_roomdetails.sys_language_uid IN (-1,0)',
            ],
        ],
        'l10n_diffsource' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
        't3ver_label' => [
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'max' => 255,
            ],
        ],
        'hidden' => [
            'exclude' => true,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
            'config' => [
                'type' => 'check',
                'items' => [
                    '1' => [
                        '0' => 'LLL:EXT:lang/Resources/Private/Language/locallang_core.xlf:labels.enabled'
                    ]
                ],
            ],
        ],
        'starttime' => [
            'exclude' => true,
            'behaviour' => [
                'allowLanguageSynchronization' => true
            ],
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'size' => 13,
                'eval' => 'datetime',
                'default' => 0,
            ],
        ],
        'endtime' => [
            'exclude' => true,
            'behaviour' => [
                'allowLanguageSynchronization' => true
            ],
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'size' => 13,
                'eval' => 'datetime',
                'default' => 0,
                'range' => [
                    'upper' => mktime(0, 0, 0, 1, 1, 2038)
                ],
            ],
        ],

        'room_key' => [
            'exclude' => true,
            'label' => 'LLL:EXT:hs_roombooking/Resources/Private/Language/locallang_db.xlf:tx_hsroombooking_domain_model_roomdetails.room_key',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'capacity' => [
            'exclude' => true,
            'label' => 'LLL:EXT:hs_roombooking/Resources/Private/Language/locallang_db.xlf:tx_hsroombooking_domain_model_roomdetails.capacity',
            'config' => [
                'type' => 'input',
                'size' => 4,
                'eval' => 'int'
            ]
        ],
        'whiteboard' => [
            'exclude' => true,
            'label' => 'LLL:EXT:hs_roombooking/Resources/Private/Language/locallang_db.xlf:tx_hsroombooking_domain_model_roomdetails.whiteboard',
            'config' => [
                'type' => 'check',
                'items' => [
                    '1' => [
                        '0' => 'LLL:EXT:lang/Resources/Private/Language/locallang_core.xlf:labels.enabled'
                    ]
                ],
                'default' => 0,
            ]
            
        ],
        'blackboard' => [
            'exclude' => true,
            'label' => 'LLL:EXT:hs_roombooking/Resources/Private/Language/locallang_db.xlf:tx_hsroombooking_domain_model_roomdetails.blackboard',
            'config' => [
                'type' => 'check',
                'items' => [
                    '1' => [
                        '0' => 'LLL:EXT:lang/Resources/Private/Language/locallang_core.xlf:labels.enabled'
                    ]
                ],
                'default' => 0,
            ]
            
        ],
        'beamer' => [
            'exclude' => true,
            'label' => 'LLL:EXT:hs_roombooking/Resources/Private/Language/locallang_db.xlf:tx_hsroombooking_domain_model_roomdetails.beamer',
            'config' => [
                'type' => 'check',
                'items' => [
                    '1' => [
                        '0' => 'LLL:EXT:lang/Resources/Private/Language/locallang_core.xlf:labels.enabled'
                    ]
                ],
                'default' => 0,
            ]
            
        ],
        'roomtype' => [
            'exclude' => true,
            'label' => 'LLL:EXT:hs_roombooking/Resources/Private/Language/locallang_db.xlf:tx_hsroombooking_domain_model_roomdetails.roomtype',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
    
        'bookingsystem' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
    ],
];
