<?php
defined('TYPO3_MODE') || die();

if (!isset($GLOBALS['TCA']['fe_users']['ctrl']['type'])) {
    // no type field defined, so we define it here. This will only happen the first time the extension is installed!!
    $GLOBALS['TCA']['fe_users']['ctrl']['type'] = 'tx_extbase_type';
    $tempColumnstx_hsroombooking_fe_users = [];
    $tempColumnstx_hsroombooking_fe_users[$GLOBALS['TCA']['fe_users']['ctrl']['type']] = [
        'exclude' => true,
        'label'   => 'LLL:EXT:hs_roombooking/Resources/Private/Language/locallang_db.xlf:tx_hsroombooking.tx_extbase_type',
        'config' => [
            'type' => 'select',
            'renderType' => 'selectSingle',
            'items' => [
                ['',''],
                ['Person','Tx_HsRoombooking_Person']
            ],
            'default' => 'Tx_HsRoombooking_Person',
            'size' => 1,
            'maxitems' => 1,
        ]
    ];
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('fe_users', $tempColumnstx_hsroombooking_fe_users);
}

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
    'fe_users',
    $GLOBALS['TCA']['fe_users']['ctrl']['type'],
    '',
    'after:' . $GLOBALS['TCA']['fe_users']['ctrl']['label']
);

$tmp_hs_roombooking_columns = [

    'firstname' => [
        'exclude' => true,
        'label' => 'LLL:EXT:hs_roombooking/Resources/Private/Language/locallang_db.xlf:tx_hsroombooking_domain_model_person.firstname',
        'config' => [
            'type' => 'input',
            'size' => 30,
            'eval' => 'trim'
        ],
    ],
    'lastname' => [
        'exclude' => true,
        'label' => 'LLL:EXT:hs_roombooking/Resources/Private/Language/locallang_db.xlf:tx_hsroombooking_domain_model_person.lastname',
        'config' => [
            'type' => 'input',
            'size' => 30,
            'eval' => 'trim'
        ],
    ],
    'email' => [
        'exclude' => true,
        'label' => 'LLL:EXT:hs_roombooking/Resources/Private/Language/locallang_db.xlf:tx_hsroombooking_domain_model_person.email',
        'config' => [
            'type' => 'input',
            'size' => 30,
            'eval' => 'trim'
        ],
    ],
    'confroom' => [
        'exclude' => true,
        'label' => 'LLL:EXT:hs_roombooking/Resources/Private/Language/locallang_db.xlf:tx_hsroombooking_domain_model_person.confroom',
        'config' => [
            'type' => 'select',
            'renderType' => 'selectMultipleSideBySide',
            'foreign_table' => 'tx_hsroombooking_domain_model_confroom',
            'MM' => 'tx_hsroombooking_person_confroom_mm',
            'size' => 10,
            'autoSizeMax' => 30,
            'maxitems' => 9999,
            'multiple' => 0,
            'fieldControl' => [
                'editPopup' => [
                    'disabled' => false,
                ],
                'addRecord' => [
                    'disabled' => false,
                ],
                'listModule' => [
                    'disabled' => true,
                ],
            ],
        ],
        
    ],
    'rooms' => [
        'exclude' => true,
        'label' => 'LLL:EXT:hs_roombooking/Resources/Private/Language/locallang_db.xlf:tx_hsroombooking_domain_model_person.rooms',
        'config' => [
            'type' => 'select',
            'renderType' => 'selectMultipleSideBySide',
            'foreign_table' => 'tx_hsroombooking_domain_model_rooms',
            'MM' => 'tx_hsroombooking_person_rooms_mm',
            'size' => 10,
            'autoSizeMax' => 30,
            'maxitems' => 9999,
            'multiple' => 0,
            'fieldControl' => [
                'editPopup' => [
                    'disabled' => false,
                ],
                'addRecord' => [
                    'disabled' => false,
                ],
                'listModule' => [
                    'disabled' => true,
                ],
            ],
        ],
        
    ],

];

$tmp_hs_roombooking_columns['bookingsystem'] = [
    'config' => [
        'type' => 'passthrough',
    ]
];

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('fe_users',$tmp_hs_roombooking_columns);

/* inherit and extend the show items from the parent class */

if (isset($GLOBALS['TCA']['fe_users']['types']['0']['showitem'])) {
    $GLOBALS['TCA']['fe_users']['types']['Tx_HsRoombooking_Person']['showitem'] = $GLOBALS['TCA']['fe_users']['types']['0']['showitem'];
} elseif(is_array($GLOBALS['TCA']['fe_users']['types'])) {
    // use first entry in types array
    $fe_users_type_definition = reset($GLOBALS['TCA']['fe_users']['types']);
    $GLOBALS['TCA']['fe_users']['types']['Tx_HsRoombooking_Person']['showitem'] = $fe_users_type_definition['showitem'];
} else {
    $GLOBALS['TCA']['fe_users']['types']['Tx_HsRoombooking_Person']['showitem'] = '';
}
$GLOBALS['TCA']['fe_users']['types']['Tx_HsRoombooking_Person']['showitem'] .= ',--div--;LLL:EXT:hs_roombooking/Resources/Private/Language/locallang_db.xlf:tx_hsroombooking_domain_model_person,';
$GLOBALS['TCA']['fe_users']['types']['Tx_HsRoombooking_Person']['showitem'] .= 'firstname, lastname, email, confroom, rooms';

$GLOBALS['TCA']['fe_users']['columns'][$GLOBALS['TCA']['fe_users']['ctrl']['type']]['config']['items'][] = ['LLL:EXT:hs_roombooking/Resources/Private/Language/locallang_db.xlf:fe_users.tx_extbase_type.Tx_HsRoombooking_Person','Tx_HsRoombooking_Person'];
