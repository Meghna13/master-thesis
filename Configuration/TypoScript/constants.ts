
plugin.tx_hsroombooking_hochschulebooking {
    view {
        # cat=plugin.tx_hsroombooking_hochschulebooking/file; type=string; label=Path to template root (FE)
        templateRootPath = EXT:hs_roombooking/Resources/Private/Templates/
        # cat=plugin.tx_hsroombooking_hochschulebooking/file; type=string; label=Path to template partials (FE)
        partialRootPath = EXT:hs_roombooking/Resources/Private/Partials/
        # cat=plugin.tx_hsroombooking_hochschulebooking/file; type=string; label=Path to template layouts (FE)
        layoutRootPath = EXT:hs_roombooking/Resources/Private/Layouts/
    }
    persistence {
        # cat=plugin.tx_hsroombooking_hochschulebooking//a; type=string; label=Default storage PID
        storagePid =
    }
}
