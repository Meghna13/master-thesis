<?php
namespace Hochschule\HsRoombooking\Tests\Unit\Domain\Model;

/**
 * Test case.
 *
 * @author Meghna Anand <meghnaa.anand@gmail.com>
 */
class PersonTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \Hochschule\HsRoombooking\Domain\Model\Person
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \Hochschule\HsRoombooking\Domain\Model\Person();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getFirstnameReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getFirstname()
        );
    }

    /**
     * @test
     */
    public function setFirstnameForStringSetsFirstname()
    {
        $this->subject->setFirstname('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'firstname',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getLastnameReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getLastname()
        );
    }

    /**
     * @test
     */
    public function setLastnameForStringSetsLastname()
    {
        $this->subject->setLastname('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'lastname',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getEmailReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getEmail()
        );
    }

    /**
     * @test
     */
    public function setEmailForStringSetsEmail()
    {
        $this->subject->setEmail('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'email',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getConfroomReturnsInitialValueForConfroom()
    {
        $newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        self::assertEquals(
            $newObjectStorage,
            $this->subject->getConfroom()
        );
    }

    /**
     * @test
     */
    public function setConfroomForObjectStorageContainingConfroomSetsConfroom()
    {
        $confroom = new \Hochschule\HsRoombooking\Domain\Model\Confroom();
        $objectStorageHoldingExactlyOneConfroom = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $objectStorageHoldingExactlyOneConfroom->attach($confroom);
        $this->subject->setConfroom($objectStorageHoldingExactlyOneConfroom);

        self::assertAttributeEquals(
            $objectStorageHoldingExactlyOneConfroom,
            'confroom',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function addConfroomToObjectStorageHoldingConfroom()
    {
        $confroom = new \Hochschule\HsRoombooking\Domain\Model\Confroom();
        $confroomObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['attach'])
            ->disableOriginalConstructor()
            ->getMock();

        $confroomObjectStorageMock->expects(self::once())->method('attach')->with(self::equalTo($confroom));
        $this->inject($this->subject, 'confroom', $confroomObjectStorageMock);

        $this->subject->addConfroom($confroom);
    }

    /**
     * @test
     */
    public function removeConfroomFromObjectStorageHoldingConfroom()
    {
        $confroom = new \Hochschule\HsRoombooking\Domain\Model\Confroom();
        $confroomObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['detach'])
            ->disableOriginalConstructor()
            ->getMock();

        $confroomObjectStorageMock->expects(self::once())->method('detach')->with(self::equalTo($confroom));
        $this->inject($this->subject, 'confroom', $confroomObjectStorageMock);

        $this->subject->removeConfroom($confroom);
    }

    /**
     * @test
     */
    public function getRoomsReturnsInitialValueForRooms()
    {
        $newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        self::assertEquals(
            $newObjectStorage,
            $this->subject->getRooms()
        );
    }

    /**
     * @test
     */
    public function setRoomsForObjectStorageContainingRoomsSetsRooms()
    {
        $room = new \Hochschule\HsRoombooking\Domain\Model\Rooms();
        $objectStorageHoldingExactlyOneRooms = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $objectStorageHoldingExactlyOneRooms->attach($room);
        $this->subject->setRooms($objectStorageHoldingExactlyOneRooms);

        self::assertAttributeEquals(
            $objectStorageHoldingExactlyOneRooms,
            'rooms',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function addRoomToObjectStorageHoldingRooms()
    {
        $room = new \Hochschule\HsRoombooking\Domain\Model\Rooms();
        $roomsObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['attach'])
            ->disableOriginalConstructor()
            ->getMock();

        $roomsObjectStorageMock->expects(self::once())->method('attach')->with(self::equalTo($room));
        $this->inject($this->subject, 'rooms', $roomsObjectStorageMock);

        $this->subject->addRoom($room);
    }

    /**
     * @test
     */
    public function removeRoomFromObjectStorageHoldingRooms()
    {
        $room = new \Hochschule\HsRoombooking\Domain\Model\Rooms();
        $roomsObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['detach'])
            ->disableOriginalConstructor()
            ->getMock();

        $roomsObjectStorageMock->expects(self::once())->method('detach')->with(self::equalTo($room));
        $this->inject($this->subject, 'rooms', $roomsObjectStorageMock);

        $this->subject->removeRoom($room);
    }
}
