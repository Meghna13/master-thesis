<?php
namespace Hochschule\HsRoombooking\Tests\Unit\Domain\Model;

/**
 * Test case.
 *
 * @author Meghna Anand <meghnaa.anand@gmail.com>
 */
class RoomdetailsTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \Hochschule\HsRoombooking\Domain\Model\Roomdetails
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \Hochschule\HsRoombooking\Domain\Model\Roomdetails();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getRoomKeyReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getRoomKey()
        );
    }

    /**
     * @test
     */
    public function setRoomKeyForStringSetsRoomKey()
    {
        $this->subject->setRoomKey('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'roomKey',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getCapacityReturnsInitialValueForInt()
    {
        self::assertSame(
            0,
            $this->subject->getCapacity()
        );
    }

    /**
     * @test
     */
    public function setCapacityForIntSetsCapacity()
    {
        $this->subject->setCapacity(12);

        self::assertAttributeEquals(
            12,
            'capacity',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getWhiteboardReturnsInitialValueForBool()
    {
        self::assertSame(
            false,
            $this->subject->getWhiteboard()
        );
    }

    /**
     * @test
     */
    public function setWhiteboardForBoolSetsWhiteboard()
    {
        $this->subject->setWhiteboard(true);

        self::assertAttributeEquals(
            true,
            'whiteboard',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getBlackboardReturnsInitialValueForBool()
    {
        self::assertSame(
            false,
            $this->subject->getBlackboard()
        );
    }

    /**
     * @test
     */
    public function setBlackboardForBoolSetsBlackboard()
    {
        $this->subject->setBlackboard(true);

        self::assertAttributeEquals(
            true,
            'blackboard',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getBeamerReturnsInitialValueForBool()
    {
        self::assertSame(
            false,
            $this->subject->getBeamer()
        );
    }

    /**
     * @test
     */
    public function setBeamerForBoolSetsBeamer()
    {
        $this->subject->setBeamer(true);

        self::assertAttributeEquals(
            true,
            'beamer',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getRoomtypeReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getRoomtype()
        );
    }

    /**
     * @test
     */
    public function setRoomtypeForStringSetsRoomtype()
    {
        $this->subject->setRoomtype('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'roomtype',
            $this->subject
        );
    }
}
