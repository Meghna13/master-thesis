<?php
namespace Hochschule\HsRoombooking\Tests\Unit\Domain\Model;

/**
 * Test case.
 *
 * @author Meghna Anand <meghnaa.anand@gmail.com>
 */
class TimerangeTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \Hochschule\HsRoombooking\Domain\Model\Timerange
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \Hochschule\HsRoombooking\Domain\Model\Timerange();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getStarttimerangeReturnsInitialValueForInt()
    {
        self::assertSame(
            0,
            $this->subject->getStarttimerange()
        );
    }

    /**
     * @test
     */
    public function setStarttimerangeForIntSetsStarttimerange()
    {
        $this->subject->setStarttimerange(12);

        self::assertAttributeEquals(
            12,
            'starttimerange',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getEndtimerangeReturnsInitialValueForInt()
    {
        self::assertSame(
            0,
            $this->subject->getEndtimerange()
        );
    }

    /**
     * @test
     */
    public function setEndtimerangeForIntSetsEndtimerange()
    {
        $this->subject->setEndtimerange(12);

        self::assertAttributeEquals(
            12,
            'endtimerange',
            $this->subject
        );
    }
}
