<?php
namespace Hochschule\HsRoombooking\Tests\Unit\Domain\Model;

/**
 * Test case.
 *
 * @author Meghna Anand <meghnaa.anand@gmail.com>
 */
class ClassroomTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \Hochschule\HsRoombooking\Domain\Model\Classroom
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \Hochschule\HsRoombooking\Domain\Model\Classroom();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getStartdatetimeReturnsInitialValueForDateTime()
    {
        self::assertEquals(
            null,
            $this->subject->getStartdatetime()
        );
    }

    /**
     * @test
     */
    public function setStartdatetimeForDateTimeSetsStartdatetime()
    {
        $dateTimeFixture = new \DateTime();
        $this->subject->setStartdatetime($dateTimeFixture);

        self::assertAttributeEquals(
            $dateTimeFixture,
            'startdatetime',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getEnddatetimeReturnsInitialValueForDateTime()
    {
        self::assertEquals(
            null,
            $this->subject->getEnddatetime()
        );
    }

    /**
     * @test
     */
    public function setEnddatetimeForDateTimeSetsEnddatetime()
    {
        $dateTimeFixture = new \DateTime();
        $this->subject->setEnddatetime($dateTimeFixture);

        self::assertAttributeEquals(
            $dateTimeFixture,
            'enddatetime',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getRoomKeyReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getRoomKey()
        );
    }

    /**
     * @test
     */
    public function setRoomKeyForStringSetsRoomKey()
    {
        $this->subject->setRoomKey('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'roomKey',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getIsBookedReturnsInitialValueForBool()
    {
        self::assertSame(
            false,
            $this->subject->getIsBooked()
        );
    }

    /**
     * @test
     */
    public function setIsBookedForBoolSetsIsBooked()
    {
        $this->subject->setIsBooked(true);

        self::assertAttributeEquals(
            true,
            'isBooked',
            $this->subject
        );
    }
}
