<?php
namespace Hochschule\HsRoombooking\Tests\Unit\Domain\Model;

/**
 * Test case.
 *
 * @author Meghna Anand <meghnaa.anand@gmail.com>
 */
class RoomsTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \Hochschule\HsRoombooking\Domain\Model\Rooms
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \Hochschule\HsRoombooking\Domain\Model\Rooms();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getStartdatetimeReturnsInitialValueForDateTime()
    {
        self::assertEquals(
            null,
            $this->subject->getStartdatetime()
        );
    }

    /**
     * @test
     */
    public function setStartdatetimeForDateTimeSetsStartdatetime()
    {
        $dateTimeFixture = new \DateTime();
        $this->subject->setStartdatetime($dateTimeFixture);

        self::assertAttributeEquals(
            $dateTimeFixture,
            'startdatetime',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getEnddatetimeReturnsInitialValueForDateTime()
    {
        self::assertEquals(
            null,
            $this->subject->getEnddatetime()
        );
    }

    /**
     * @test
     */
    public function setEnddatetimeForDateTimeSetsEnddatetime()
    {
        $dateTimeFixture = new \DateTime();
        $this->subject->setEnddatetime($dateTimeFixture);

        self::assertAttributeEquals(
            $dateTimeFixture,
            'enddatetime',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getRoomKeyReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getRoomKey()
        );
    }

    /**
     * @test
     */
    public function setRoomKeyForStringSetsRoomKey()
    {
        $this->subject->setRoomKey('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'roomKey',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getRoomtypeReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getRoomtype()
        );
    }

    /**
     * @test
     */
    public function setRoomtypeForStringSetsRoomtype()
    {
        $this->subject->setRoomtype('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'roomtype',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getIsBookedReturnsInitialValueForBool()
    {
        self::assertSame(
            false,
            $this->subject->getIsBooked()
        );
    }

    /**
     * @test
     */
    public function setIsBookedForBoolSetsIsBooked()
    {
        $this->subject->setIsBooked(true);

        self::assertAttributeEquals(
            true,
            'isBooked',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getRoomdetailsReturnsInitialValueForRoomdetails()
    {
        self::assertEquals(
            null,
            $this->subject->getRoomdetails()
        );
    }

    /**
     * @test
     */
    public function setRoomdetailsForRoomdetailsSetsRoomdetails()
    {
        $roomdetailsFixture = new \Hochschule\HsRoombooking\Domain\Model\Roomdetails();
        $this->subject->setRoomdetails($roomdetailsFixture);

        self::assertAttributeEquals(
            $roomdetailsFixture,
            'roomdetails',
            $this->subject
        );
    }
}
