<?php
namespace Hochschule\HsRoombooking\Tests\Unit\Domain\Model;

/**
 * Test case.
 *
 * @author Meghna Anand <meghnaa.anand@gmail.com>
 */
class BookingsystemTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \Hochschule\HsRoombooking\Domain\Model\Bookingsystem
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \Hochschule\HsRoombooking\Domain\Model\Bookingsystem();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getRoomsReturnsInitialValueForRooms()
    {
        $newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        self::assertEquals(
            $newObjectStorage,
            $this->subject->getRooms()
        );
    }

    /**
     * @test
     */
    public function setRoomsForObjectStorageContainingRoomsSetsRooms()
    {
        $room = new \Hochschule\HsRoombooking\Domain\Model\Rooms();
        $objectStorageHoldingExactlyOneRooms = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $objectStorageHoldingExactlyOneRooms->attach($room);
        $this->subject->setRooms($objectStorageHoldingExactlyOneRooms);

        self::assertAttributeEquals(
            $objectStorageHoldingExactlyOneRooms,
            'rooms',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function addRoomToObjectStorageHoldingRooms()
    {
        $room = new \Hochschule\HsRoombooking\Domain\Model\Rooms();
        $roomsObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['attach'])
            ->disableOriginalConstructor()
            ->getMock();

        $roomsObjectStorageMock->expects(self::once())->method('attach')->with(self::equalTo($room));
        $this->inject($this->subject, 'rooms', $roomsObjectStorageMock);

        $this->subject->addRoom($room);
    }

    /**
     * @test
     */
    public function removeRoomFromObjectStorageHoldingRooms()
    {
        $room = new \Hochschule\HsRoombooking\Domain\Model\Rooms();
        $roomsObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['detach'])
            ->disableOriginalConstructor()
            ->getMock();

        $roomsObjectStorageMock->expects(self::once())->method('detach')->with(self::equalTo($room));
        $this->inject($this->subject, 'rooms', $roomsObjectStorageMock);

        $this->subject->removeRoom($room);
    }

    /**
     * @test
     */
    public function getRoomDetailsReturnsInitialValueForRoomdetails()
    {
        $newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        self::assertEquals(
            $newObjectStorage,
            $this->subject->getRoomDetails()
        );
    }

    /**
     * @test
     */
    public function setRoomDetailsForObjectStorageContainingRoomdetailsSetsRoomDetails()
    {
        $roomDetail = new \Hochschule\HsRoombooking\Domain\Model\Roomdetails();
        $objectStorageHoldingExactlyOneRoomDetails = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $objectStorageHoldingExactlyOneRoomDetails->attach($roomDetail);
        $this->subject->setRoomDetails($objectStorageHoldingExactlyOneRoomDetails);

        self::assertAttributeEquals(
            $objectStorageHoldingExactlyOneRoomDetails,
            'roomDetails',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function addRoomDetailToObjectStorageHoldingRoomDetails()
    {
        $roomDetail = new \Hochschule\HsRoombooking\Domain\Model\Roomdetails();
        $roomDetailsObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['attach'])
            ->disableOriginalConstructor()
            ->getMock();

        $roomDetailsObjectStorageMock->expects(self::once())->method('attach')->with(self::equalTo($roomDetail));
        $this->inject($this->subject, 'roomDetails', $roomDetailsObjectStorageMock);

        $this->subject->addRoomDetail($roomDetail);
    }

    /**
     * @test
     */
    public function removeRoomDetailFromObjectStorageHoldingRoomDetails()
    {
        $roomDetail = new \Hochschule\HsRoombooking\Domain\Model\Roomdetails();
        $roomDetailsObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['detach'])
            ->disableOriginalConstructor()
            ->getMock();

        $roomDetailsObjectStorageMock->expects(self::once())->method('detach')->with(self::equalTo($roomDetail));
        $this->inject($this->subject, 'roomDetails', $roomDetailsObjectStorageMock);

        $this->subject->removeRoomDetail($roomDetail);
    }
}
