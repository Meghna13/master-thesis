<?php
namespace Hochschule\HsRoombooking\Tests\Unit\Controller;

/**
 * Test case.
 *
 * @author Meghna Anand <meghnaa.anand@gmail.com>
 */
class ConfroomControllerTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \Hochschule\HsRoombooking\Controller\ConfroomController
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = $this->getMockBuilder(\Hochschule\HsRoombooking\Controller\ConfroomController::class)
            ->setMethods(['redirect', 'forward', 'addFlashMessage'])
            ->disableOriginalConstructor()
            ->getMock();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function listActionFetchesAllConfroomsFromRepositoryAndAssignsThemToView()
    {

        $allConfrooms = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->disableOriginalConstructor()
            ->getMock();

        $confroomRepository = $this->getMockBuilder(\Hochschule\HsRoombooking\Domain\Repository\ConfroomRepository::class)
            ->setMethods(['findAll'])
            ->disableOriginalConstructor()
            ->getMock();
        $confroomRepository->expects(self::once())->method('findAll')->will(self::returnValue($allConfrooms));
        $this->inject($this->subject, 'confroomRepository', $confroomRepository);

        $view = $this->getMockBuilder(\TYPO3\CMS\Extbase\Mvc\View\ViewInterface::class)->getMock();
        $view->expects(self::once())->method('assign')->with('confrooms', $allConfrooms);
        $this->inject($this->subject, 'view', $view);

        $this->subject->listAction();
    }

    /**
     * @test
     */
    public function showActionAssignsTheGivenConfroomToView()
    {
        $confroom = new \Hochschule\HsRoombooking\Domain\Model\Confroom();

        $view = $this->getMockBuilder(\TYPO3\CMS\Extbase\Mvc\View\ViewInterface::class)->getMock();
        $this->inject($this->subject, 'view', $view);
        $view->expects(self::once())->method('assign')->with('confroom', $confroom);

        $this->subject->showAction($confroom);
    }

    /**
     * @test
     */
    public function createActionAddsTheGivenConfroomToConfroomRepository()
    {
        $confroom = new \Hochschule\HsRoombooking\Domain\Model\Confroom();

        $confroomRepository = $this->getMockBuilder(\Hochschule\HsRoombooking\Domain\Repository\ConfroomRepository::class)
            ->setMethods(['add'])
            ->disableOriginalConstructor()
            ->getMock();

        $confroomRepository->expects(self::once())->method('add')->with($confroom);
        $this->inject($this->subject, 'confroomRepository', $confroomRepository);

        $this->subject->createAction($confroom);
    }

    /**
     * @test
     */
    public function editActionAssignsTheGivenConfroomToView()
    {
        $confroom = new \Hochschule\HsRoombooking\Domain\Model\Confroom();

        $view = $this->getMockBuilder(\TYPO3\CMS\Extbase\Mvc\View\ViewInterface::class)->getMock();
        $this->inject($this->subject, 'view', $view);
        $view->expects(self::once())->method('assign')->with('confroom', $confroom);

        $this->subject->editAction($confroom);
    }

    /**
     * @test
     */
    public function updateActionUpdatesTheGivenConfroomInConfroomRepository()
    {
        $confroom = new \Hochschule\HsRoombooking\Domain\Model\Confroom();

        $confroomRepository = $this->getMockBuilder(\Hochschule\HsRoombooking\Domain\Repository\ConfroomRepository::class)
            ->setMethods(['update'])
            ->disableOriginalConstructor()
            ->getMock();

        $confroomRepository->expects(self::once())->method('update')->with($confroom);
        $this->inject($this->subject, 'confroomRepository', $confroomRepository);

        $this->subject->updateAction($confroom);
    }

    /**
     * @test
     */
    public function deleteActionRemovesTheGivenConfroomFromConfroomRepository()
    {
        $confroom = new \Hochschule\HsRoombooking\Domain\Model\Confroom();

        $confroomRepository = $this->getMockBuilder(\Hochschule\HsRoombooking\Domain\Repository\ConfroomRepository::class)
            ->setMethods(['remove'])
            ->disableOriginalConstructor()
            ->getMock();

        $confroomRepository->expects(self::once())->method('remove')->with($confroom);
        $this->inject($this->subject, 'confroomRepository', $confroomRepository);

        $this->subject->deleteAction($confroom);
    }
}
