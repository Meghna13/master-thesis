<?php
namespace Hochschule\HsRoombooking\Tests\Unit\Controller;

/**
 * Test case.
 *
 * @author Meghna Anand <meghnaa.anand@gmail.com>
 */
class RoomdetailsControllerTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \Hochschule\HsRoombooking\Controller\RoomdetailsController
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = $this->getMockBuilder(\Hochschule\HsRoombooking\Controller\RoomdetailsController::class)
            ->setMethods(['redirect', 'forward', 'addFlashMessage'])
            ->disableOriginalConstructor()
            ->getMock();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function listActionFetchesAllRoomdetailssFromRepositoryAndAssignsThemToView()
    {

        $allRoomdetailss = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->disableOriginalConstructor()
            ->getMock();

        $roomdetailsRepository = $this->getMockBuilder(\Hochschule\HsRoombooking\Domain\Repository\RoomdetailsRepository::class)
            ->setMethods(['findAll'])
            ->disableOriginalConstructor()
            ->getMock();
        $roomdetailsRepository->expects(self::once())->method('findAll')->will(self::returnValue($allRoomdetailss));
        $this->inject($this->subject, 'roomdetailsRepository', $roomdetailsRepository);

        $view = $this->getMockBuilder(\TYPO3\CMS\Extbase\Mvc\View\ViewInterface::class)->getMock();
        $view->expects(self::once())->method('assign')->with('roomdetailss', $allRoomdetailss);
        $this->inject($this->subject, 'view', $view);

        $this->subject->listAction();
    }

    /**
     * @test
     */
    public function showActionAssignsTheGivenRoomdetailsToView()
    {
        $roomdetails = new \Hochschule\HsRoombooking\Domain\Model\Roomdetails();

        $view = $this->getMockBuilder(\TYPO3\CMS\Extbase\Mvc\View\ViewInterface::class)->getMock();
        $this->inject($this->subject, 'view', $view);
        $view->expects(self::once())->method('assign')->with('roomdetails', $roomdetails);

        $this->subject->showAction($roomdetails);
    }

    /**
     * @test
     */
    public function createActionAddsTheGivenRoomdetailsToRoomdetailsRepository()
    {
        $roomdetails = new \Hochschule\HsRoombooking\Domain\Model\Roomdetails();

        $roomdetailsRepository = $this->getMockBuilder(\Hochschule\HsRoombooking\Domain\Repository\RoomdetailsRepository::class)
            ->setMethods(['add'])
            ->disableOriginalConstructor()
            ->getMock();

        $roomdetailsRepository->expects(self::once())->method('add')->with($roomdetails);
        $this->inject($this->subject, 'roomdetailsRepository', $roomdetailsRepository);

        $this->subject->createAction($roomdetails);
    }

    /**
     * @test
     */
    public function editActionAssignsTheGivenRoomdetailsToView()
    {
        $roomdetails = new \Hochschule\HsRoombooking\Domain\Model\Roomdetails();

        $view = $this->getMockBuilder(\TYPO3\CMS\Extbase\Mvc\View\ViewInterface::class)->getMock();
        $this->inject($this->subject, 'view', $view);
        $view->expects(self::once())->method('assign')->with('roomdetails', $roomdetails);

        $this->subject->editAction($roomdetails);
    }

    /**
     * @test
     */
    public function updateActionUpdatesTheGivenRoomdetailsInRoomdetailsRepository()
    {
        $roomdetails = new \Hochschule\HsRoombooking\Domain\Model\Roomdetails();

        $roomdetailsRepository = $this->getMockBuilder(\Hochschule\HsRoombooking\Domain\Repository\RoomdetailsRepository::class)
            ->setMethods(['update'])
            ->disableOriginalConstructor()
            ->getMock();

        $roomdetailsRepository->expects(self::once())->method('update')->with($roomdetails);
        $this->inject($this->subject, 'roomdetailsRepository', $roomdetailsRepository);

        $this->subject->updateAction($roomdetails);
    }
}
