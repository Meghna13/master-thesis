<?php
namespace Hochschule\HsRoombooking\Tests\Unit\Controller;

/**
 * Test case.
 *
 * @author Meghna Anand <meghnaa.anand@gmail.com>
 */
class RoomsControllerTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \Hochschule\HsRoombooking\Controller\RoomsController
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = $this->getMockBuilder(\Hochschule\HsRoombooking\Controller\RoomsController::class)
            ->setMethods(['redirect', 'forward', 'addFlashMessage'])
            ->disableOriginalConstructor()
            ->getMock();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function listActionFetchesAllRoomssFromRepositoryAndAssignsThemToView()
    {

        $allRoomss = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->disableOriginalConstructor()
            ->getMock();

        $roomsRepository = $this->getMockBuilder(\Hochschule\HsRoombooking\Domain\Repository\RoomsRepository::class)
            ->setMethods(['findAll'])
            ->disableOriginalConstructor()
            ->getMock();
        $roomsRepository->expects(self::once())->method('findAll')->will(self::returnValue($allRoomss));
        $this->inject($this->subject, 'roomsRepository', $roomsRepository);

        $view = $this->getMockBuilder(\TYPO3\CMS\Extbase\Mvc\View\ViewInterface::class)->getMock();
        $view->expects(self::once())->method('assign')->with('roomss', $allRoomss);
        $this->inject($this->subject, 'view', $view);

        $this->subject->listAction();
    }

    /**
     * @test
     */
    public function showActionAssignsTheGivenRoomsToView()
    {
        $rooms = new \Hochschule\HsRoombooking\Domain\Model\Rooms();

        $view = $this->getMockBuilder(\TYPO3\CMS\Extbase\Mvc\View\ViewInterface::class)->getMock();
        $this->inject($this->subject, 'view', $view);
        $view->expects(self::once())->method('assign')->with('rooms', $rooms);

        $this->subject->showAction($rooms);
    }

    /**
     * @test
     */
    public function createActionAddsTheGivenRoomsToRoomsRepository()
    {
        $rooms = new \Hochschule\HsRoombooking\Domain\Model\Rooms();

        $roomsRepository = $this->getMockBuilder(\Hochschule\HsRoombooking\Domain\Repository\RoomsRepository::class)
            ->setMethods(['add'])
            ->disableOriginalConstructor()
            ->getMock();

        $roomsRepository->expects(self::once())->method('add')->with($rooms);
        $this->inject($this->subject, 'roomsRepository', $roomsRepository);

        $this->subject->createAction($rooms);
    }

    /**
     * @test
     */
    public function editActionAssignsTheGivenRoomsToView()
    {
        $rooms = new \Hochschule\HsRoombooking\Domain\Model\Rooms();

        $view = $this->getMockBuilder(\TYPO3\CMS\Extbase\Mvc\View\ViewInterface::class)->getMock();
        $this->inject($this->subject, 'view', $view);
        $view->expects(self::once())->method('assign')->with('rooms', $rooms);

        $this->subject->editAction($rooms);
    }

    /**
     * @test
     */
    public function updateActionUpdatesTheGivenRoomsInRoomsRepository()
    {
        $rooms = new \Hochschule\HsRoombooking\Domain\Model\Rooms();

        $roomsRepository = $this->getMockBuilder(\Hochschule\HsRoombooking\Domain\Repository\RoomsRepository::class)
            ->setMethods(['update'])
            ->disableOriginalConstructor()
            ->getMock();

        $roomsRepository->expects(self::once())->method('update')->with($rooms);
        $this->inject($this->subject, 'roomsRepository', $roomsRepository);

        $this->subject->updateAction($rooms);
    }
}
