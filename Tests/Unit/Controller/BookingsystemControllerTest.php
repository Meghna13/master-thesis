<?php
namespace Hochschule\HsRoombooking\Tests\Unit\Controller;

/**
 * Test case.
 *
 * @author Meghna Anand <meghnaa.anand@gmail.com>
 */
class BookingsystemControllerTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \Hochschule\HsRoombooking\Controller\BookingsystemController
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = $this->getMockBuilder(\Hochschule\HsRoombooking\Controller\BookingsystemController::class)
            ->setMethods(['redirect', 'forward', 'addFlashMessage'])
            ->disableOriginalConstructor()
            ->getMock();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function listActionFetchesAllBookingsystemsFromRepositoryAndAssignsThemToView()
    {

        $allBookingsystems = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->disableOriginalConstructor()
            ->getMock();

        $bookingsystemRepository = $this->getMockBuilder(\Hochschule\HsRoombooking\Domain\Repository\BookingsystemRepository::class)
            ->setMethods(['findAll'])
            ->disableOriginalConstructor()
            ->getMock();
        $bookingsystemRepository->expects(self::once())->method('findAll')->will(self::returnValue($allBookingsystems));
        $this->inject($this->subject, 'bookingsystemRepository', $bookingsystemRepository);

        $view = $this->getMockBuilder(\TYPO3\CMS\Extbase\Mvc\View\ViewInterface::class)->getMock();
        $view->expects(self::once())->method('assign')->with('bookingsystems', $allBookingsystems);
        $this->inject($this->subject, 'view', $view);

        $this->subject->listAction();
    }

    /**
     * @test
     */
    public function showActionAssignsTheGivenBookingsystemToView()
    {
        $bookingsystem = new \Hochschule\HsRoombooking\Domain\Model\Bookingsystem();

        $view = $this->getMockBuilder(\TYPO3\CMS\Extbase\Mvc\View\ViewInterface::class)->getMock();
        $this->inject($this->subject, 'view', $view);
        $view->expects(self::once())->method('assign')->with('bookingsystem', $bookingsystem);

        $this->subject->showAction($bookingsystem);
    }

    /**
     * @test
     */
    public function createActionAddsTheGivenBookingsystemToBookingsystemRepository()
    {
        $bookingsystem = new \Hochschule\HsRoombooking\Domain\Model\Bookingsystem();

        $bookingsystemRepository = $this->getMockBuilder(\Hochschule\HsRoombooking\Domain\Repository\BookingsystemRepository::class)
            ->setMethods(['add'])
            ->disableOriginalConstructor()
            ->getMock();

        $bookingsystemRepository->expects(self::once())->method('add')->with($bookingsystem);
        $this->inject($this->subject, 'bookingsystemRepository', $bookingsystemRepository);

        $this->subject->createAction($bookingsystem);
    }

    /**
     * @test
     */
    public function editActionAssignsTheGivenBookingsystemToView()
    {
        $bookingsystem = new \Hochschule\HsRoombooking\Domain\Model\Bookingsystem();

        $view = $this->getMockBuilder(\TYPO3\CMS\Extbase\Mvc\View\ViewInterface::class)->getMock();
        $this->inject($this->subject, 'view', $view);
        $view->expects(self::once())->method('assign')->with('bookingsystem', $bookingsystem);

        $this->subject->editAction($bookingsystem);
    }

    /**
     * @test
     */
    public function updateActionUpdatesTheGivenBookingsystemInBookingsystemRepository()
    {
        $bookingsystem = new \Hochschule\HsRoombooking\Domain\Model\Bookingsystem();

        $bookingsystemRepository = $this->getMockBuilder(\Hochschule\HsRoombooking\Domain\Repository\BookingsystemRepository::class)
            ->setMethods(['update'])
            ->disableOriginalConstructor()
            ->getMock();

        $bookingsystemRepository->expects(self::once())->method('update')->with($bookingsystem);
        $this->inject($this->subject, 'bookingsystemRepository', $bookingsystemRepository);

        $this->subject->updateAction($bookingsystem);
    }

    /**
     * @test
     */
    public function deleteActionRemovesTheGivenBookingsystemFromBookingsystemRepository()
    {
        $bookingsystem = new \Hochschule\HsRoombooking\Domain\Model\Bookingsystem();

        $bookingsystemRepository = $this->getMockBuilder(\Hochschule\HsRoombooking\Domain\Repository\BookingsystemRepository::class)
            ->setMethods(['remove'])
            ->disableOriginalConstructor()
            ->getMock();

        $bookingsystemRepository->expects(self::once())->method('remove')->with($bookingsystem);
        $this->inject($this->subject, 'bookingsystemRepository', $bookingsystemRepository);

        $this->subject->deleteAction($bookingsystem);
    }
}
