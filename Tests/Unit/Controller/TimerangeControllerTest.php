<?php
namespace Hochschule\HsRoombooking\Tests\Unit\Controller;

/**
 * Test case.
 *
 * @author Meghna Anand <meghnaa.anand@gmail.com>
 */
class TimerangeControllerTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \Hochschule\HsRoombooking\Controller\TimerangeController
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = $this->getMockBuilder(\Hochschule\HsRoombooking\Controller\TimerangeController::class)
            ->setMethods(['redirect', 'forward', 'addFlashMessage'])
            ->disableOriginalConstructor()
            ->getMock();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function listActionFetchesAllTimerangesFromRepositoryAndAssignsThemToView()
    {

        $allTimeranges = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->disableOriginalConstructor()
            ->getMock();

        $timerangeRepository = $this->getMockBuilder(\Hochschule\HsRoombooking\Domain\Repository\TimerangeRepository::class)
            ->setMethods(['findAll'])
            ->disableOriginalConstructor()
            ->getMock();
        $timerangeRepository->expects(self::once())->method('findAll')->will(self::returnValue($allTimeranges));
        $this->inject($this->subject, 'timerangeRepository', $timerangeRepository);

        $view = $this->getMockBuilder(\TYPO3\CMS\Extbase\Mvc\View\ViewInterface::class)->getMock();
        $view->expects(self::once())->method('assign')->with('timeranges', $allTimeranges);
        $this->inject($this->subject, 'view', $view);

        $this->subject->listAction();
    }

    /**
     * @test
     */
    public function showActionAssignsTheGivenTimerangeToView()
    {
        $timerange = new \Hochschule\HsRoombooking\Domain\Model\Timerange();

        $view = $this->getMockBuilder(\TYPO3\CMS\Extbase\Mvc\View\ViewInterface::class)->getMock();
        $this->inject($this->subject, 'view', $view);
        $view->expects(self::once())->method('assign')->with('timerange', $timerange);

        $this->subject->showAction($timerange);
    }

    /**
     * @test
     */
    public function createActionAddsTheGivenTimerangeToTimerangeRepository()
    {
        $timerange = new \Hochschule\HsRoombooking\Domain\Model\Timerange();

        $timerangeRepository = $this->getMockBuilder(\Hochschule\HsRoombooking\Domain\Repository\TimerangeRepository::class)
            ->setMethods(['add'])
            ->disableOriginalConstructor()
            ->getMock();

        $timerangeRepository->expects(self::once())->method('add')->with($timerange);
        $this->inject($this->subject, 'timerangeRepository', $timerangeRepository);

        $this->subject->createAction($timerange);
    }
}
