<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
	function () {

		\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
			'Hochschule.HsRoombooking',
			'Hochschulebooking',
			[
				'Bookingsystem' => 'list, show, new, book, confirmation, user,completed,authorized, accepted, process, rejected,create, edit, update, delete',
				'Roomdetails' => 'list, show, new, create, edit, update',
			],
			// non-cacheable actions
			[
				'Bookingsystem' => 'list, show, new, book, confirmation, user,completed,authorized, accepted, process, rejected,create, edit, update, delete',
				'Roomdetails' => 'create, update',
			]
		);

		// wizards
		\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
			'mod {
            wizards.newContentElement.wizardItems.plugins {
                elements {
                    hochschulebooking {
                        iconIdentifier = hs_roombooking-plugin-hochschulebooking
                        title = LLL:EXT:hs_roombooking/Resources/Private/Language/locallang_db.xlf:tx_hs_roombooking_hochschulebooking.name
                        description = LLL:EXT:hs_roombooking/Resources/Private/Language/locallang_db.xlf:tx_hs_roombooking_hochschulebooking.description
                        tt_content_defValues {
                            CType = list
                            list_type = hsroombooking_hochschulebooking
                        }
                    }
                }
                show = *
            }
       }'
		);
		$iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);

		$iconRegistry->registerIcon(
			'hs_roombooking-plugin-hochschulebooking',
			\TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
			['source' => 'EXT:hs_roombooking/Resources/Public/Icons/user_plugin_hochschulebooking.svg']
		);

	}
);

$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['extbase']['commandControllers'][] = 'Hochschule\\HsRoombooking\\Command\\XMLFetcherCommandController';

$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['extbase']['commandControllers'][] = 'Hochschule\\HsRoombooking\\Command\\XMLConfroomFetcherCommandController';

$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['extbase']['commandControllers'][] = 'Hochschule\\HsRoombooking\\Command\\XMLRoomdetailFetcherCommandController';
